module.exports = {
  purge: ['./public/**/*.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
     screens: {
      sm: '480px',
      md: '768px',
      lg: '976px',
      xl: '1440px',
    },
    extend: {
      fontFamily: {
        'serif': ['Superclarendon', 'Georgia', 'Cambria', 'Times New Roman']
      },
      colors:{
        'trasp-white': 'hsl(0deg 0% 100% / 80%)',
        orange:{
          50 : '#fffbeb',
          100: '#fef3c7',
          200: '#fde68a',
          300: '#fcd34d',
          400: '#fbbf24',
          500: '#f97316',
          600: '#ca8a04',
          700: '#a16207',
          800: '#854d0e',
          900: '#713f12',
        }
      }
    
    },
    container:{
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
